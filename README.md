# Log42

A simple wrapper for the standard log library that prefixes and color codes log messages.

## Use

```go
package main

import (
	"fmt"

	"gitlab.com/BenSchuddeboom/log42"
)

func main() {
    // Instantiate a new logger
    logger := log42.NewLogger()

    // Use:
	logger.Debugln("Test debugln")
	logger.Debugf("Test debugf")
	logger.Debug("Test debug")

	logger.Errorln("Test errorln")
	logger.Warnln("Test warnln")
	logger.Println("Test println")
	logger.Fatalln("Test fatalln")

	// If you need to use the default logger for some reason:
	logger.Logger.Panic("Panic not supported by wrapper")
}
```

Output:

![](_examples/example.png)
