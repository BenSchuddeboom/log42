package log42

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gookit/color"
)

type logWriter struct {
	color  *color.Theme
	prefix string
}

var (
	warnWriter  logWriter
	debugWriter logWriter
	infoWriter  logWriter
	fatalWriter logWriter
	errorWriter logWriter
)

// Logger is a simple wrapper for log.Logger
type Logger struct {
	log.Logger
}

func init() {
	errorWriter = logWriter{
		color:  color.Danger,
		prefix: "[ERROR] ",
	}
	fatalWriter = logWriter{
		color:  color.Error,
		prefix: "[FATAL] ",
	}
	debugWriter = logWriter{
		color:  color.Debug,
		prefix: "[DEBUG] ",
	}
	infoWriter = logWriter{
		color:  color.Info,
		prefix: "[INFO] ",
	}
	warnWriter = logWriter{
		color:  color.Warn,
		prefix: "[WARN] ",
	}
}

// Write satisfies writer interface
func (writer logWriter) Write(b []byte) (int, error) {
	s := writer.color.Sprint(writer.prefix, time.Now().Format("2006/01/02 15:04:05"), " - ", string(b))
	return fmt.Print(s)
}

// NewLogger returns a new logger
func NewLogger() *Logger {
	return new(Logger)
}

// Debugf behaves like printf but with a debug-like style
func (l *Logger) Debugf(format string, v ...interface{}) {
	l.SetOutput(debugWriter)
	l.Output(2, fmt.Sprintf(format, v...))
}

// Debugln behaves like println but with a debug-like style
func (l *Logger) Debugln(v ...interface{}) {
	l.SetOutput(debugWriter)
	l.Output(2, fmt.Sprint(v...))
}

// Debug behaves like print but with a debug-like style
func (l *Logger) Debug(v ...interface{}) {
	l.SetOutput(debugWriter)
	l.Output(2, fmt.Sprintln(v...))

}

// Fatalf behaves like fatalf but with a Fatal-like style
func (l *Logger) Fatalf(format string, v ...interface{}) {
	l.SetOutput(fatalWriter)
	l.Output(2, fmt.Sprintf(format, v...))
	os.Exit(1)
}

// Fatalln behaves like fatalln but with a Fatal-like style
func (l *Logger) Fatalln(v ...interface{}) {
	l.SetOutput(fatalWriter)
	l.Output(2, fmt.Sprintln(v...))
	os.Exit(1)
}

// Fatal behaves like fatal but with a Fatal-like style
func (l *Logger) Fatal(v ...interface{}) {
	l.SetOutput(fatalWriter)
	l.Output(2, fmt.Sprint(v...))
	os.Exit(1)
}

// Printf behaves like printf but with an info-like style
func (l *Logger) Printf(format string, v ...interface{}) {
	l.SetOutput(infoWriter)
	l.Output(2, fmt.Sprintf(format, v...))
}

// Println behaves like println but with an info-like style
func (l *Logger) Println(v ...interface{}) {
	l.SetOutput(infoWriter)
	l.Output(2, fmt.Sprint(v...))
}

// Print behaves like print but with an info-like style
func (l *Logger) Print(v ...interface{}) {
	l.SetOutput(infoWriter)
	l.Output(2, fmt.Sprint(v...))
}

// Errorf behaves like printf but with an error-like style
func (l *Logger) Errorf(format string, v ...interface{}) {
	l.SetOutput(errorWriter)
	l.Output(2, fmt.Sprintf(format, v...))
}

// Errorln behaves like println but with an error-like style
func (l *Logger) Errorln(v ...interface{}) {
	l.SetOutput(errorWriter)
	l.Output(2, fmt.Sprintln(v...))
}

// Error behaves like print but with a error-like style
func (l *Logger) Error(v ...interface{}) {
	l.SetOutput(errorWriter)
	l.Output(2, fmt.Sprint(v...))
}

// Warnf behaves like printf but with a warning-like style
func (l *Logger) Warnf(format string, v ...interface{}) {
	l.SetOutput(warnWriter)
	l.Output(2, fmt.Sprintf(format, v...))
}

// Warnln behaves like println but with a warning-like style
func (l *Logger) Warnln(v ...interface{}) {
	l.SetOutput(warnWriter)
	l.Output(2, fmt.Sprintln(v...))
}

// Warn behaves like print but with a warning-like style
func (l *Logger) Warn(v ...interface{}) {
	l.SetOutput(warnWriter)
	l.Output(2, fmt.Sprint(v...))
}
