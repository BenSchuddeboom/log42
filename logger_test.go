package log42

import "testing"

func TestLogger(*testing.T) {
	logger := NewLogger()
	logger.Debugln("Test debugln")
	logger.Errorln("Test errorln")
	logger.Warnln("Test warnln")
	logger.Println("Test println")
	logger.Logger.Panicf("Hello!")
	logger.Fatalln("Test fatalln")
}
